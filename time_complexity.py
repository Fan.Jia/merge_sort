import random
import time
import merge_sort as ms

n = 64000 

list_best = list(range(n))  
list_worst = list(range(n, -1, -1)) 
list_ave_random = [random.randint(1, n) for b in range(n)]  
         
list_ave_1_half = [random.randint(1, n//2) for b in range(n//2)] + list(range(n//2,n))
list_ave_2_half = list(range(n//2,n)) + [random.randint(1, n//2) for b in range(n//2)]
         


start_time = time.time()
ms.merge_sort(list_worst)  
end_time = time.time()
total_time = end_time - start_time

print(f"Time taken to sort list of {n} elements: " + str(total_time))