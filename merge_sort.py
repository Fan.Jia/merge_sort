def merge_sort(alist):
    if len(alist) <= 1:
        return alist
    mid = len(alist) // 2
    left = merge_sort(alist[:mid])
    right = merge_sort(alist[mid:])
    return merge(left, right)

def merge(left,right):
    result = []
    l = 0 
    r = 0
    while True:
        if left[l] < right[r]:
            result.append(left[l])
            l += 1
        else:
            result.append(right[r])
            r += 1
        if l == len(left):
            result += right[r:]
            break
        if r == len(right):
            result += left[l:]
            break
    return result